import { Component } from '@angular/core';
import { DataLocalService } from '../../services/data-local.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  constructor(
    public dataLocalService: DataLocalService,
    private iab: InAppBrowser
    ) {}

  enviarCorreo() {
    console.log('Enviando correo');
    this.dataLocalService.enviarCorreo();
  }

  mostrarRegistro(registro) {
    console.log('Registro: ', registro)
    this.dataLocalService.abrirRegistro(registro);
  }
}
