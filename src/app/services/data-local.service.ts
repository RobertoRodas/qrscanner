import { Injectable } from '@angular/core';
import { Registro } from '../models/registro.model';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { File } from '@ionic-native/file/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  guardados: Registro[] = [];

  constructor(
    private storage: Storage,
    private navCtrl: NavController,
    private iab: InAppBrowser,
    private file: File,
    private emailComposer: EmailComposer
    ) { 
      this.cargarStorage();
  }

  async cargarStorage() {
    this.guardados = (await this.storage.get('registro')) || [];
  }

  async guardarRegistros(format: string, text: string) {
    
    await this.cargarStorage();
    
    const nuevoRegistro = new Registro(format, text);

    this.guardados.unshift(nuevoRegistro);
    this.storage.set('registro', this.guardados);
    this.abrirRegistro(nuevoRegistro);
    console.log('DatosGuardados: ', this.guardados);
  }

  abrirRegistro(registro: Registro) {
    this.navCtrl.navigateForward('tabs/tab2');
    switch (registro.type) {
      case 'http':
        this.iab.create(registro.text, '_system');
        break;
      case 'geo':
        this.navCtrl.navigateForward('tabs/tab2/mapa/' + registro.text);
          break;
      default:
        break;
    }
  }


  enviarCorreo() {
    const arregloTem = [];
    const titulos = 'Tipo, Formato, Creando en, Texto\n';

    arregloTem.push(titulos);

    this.guardados.forEach( registro => {

      const linea = registro.type + ',' + registro.format + ',' + registro.created + ',' + 
      registro.text.replace(',', ' ') + '\n';

      arregloTem.push(linea);
    });

    // console.log( arregloTem.join(' '))
    this.crearArchivoFisico(arregloTem.join(' '));
  }


  crearArchivoFisico(text: string) {
    this.file.checkFile( this.file.dataDirectory, 'registro.csv')
    .then(existe => {
      console.log('Existe archivo? ', existe);
      return this.escribirEnArchivo(text);
    })
    .catch( err => {
      return this.file.createFile(this.file.dataDirectory, 'registro.csv', false)
      .then(creado => this.escribirEnArchivo (text) )
      .catch(err => console.log('NO se puede crear el archivo'))
    });

  }
  
  async escribirEnArchivo(text: string) {
    await this.file.writeExistingFile(this.file.dataDirectory, 'registro.csv', text);
    console.log('Archivo creado');
    const arcihvo = this.file.dataDirectory + 'registro.csv';
    // console.log(this.file.dataDirectory + 'registro.csv');

    const email = {
      to: 'robert28mtz@gmail.com',
      // cc: 'erika@mustermann.de',
      // bcc: ['john@doe.com', 'jane@doe.com'],
      attachments: [
        arcihvo
      ],
      subject: 'Backuo de scans',
      body: 'Aqui tiene su backuo de los scans - ' +
      ' <strong>ScanApp</strong>',
      isHtml: true
    }

    this.emailComposer.open(email);
    
  }

}
